#!/bin/bash

mkdir -p .work

rm -f ./vars.yaml ./.work/vars*

# hack to force creation of vars.yaml by halting a non-existent VM
vagrant halt setup &> /dev/null

[[ -f ./.work/vars.sh ]] && source ./.work/vars.sh

#echo $NAME

if [[ ! -z ${NAME} && -e ./.vagrant/machines/${NAME}/virtualbox/index_uuid ]]; then

    echo VM already created...
#    echo "Run \`vagrant reload $NAME\` to start or restart the VM".
    exit

fi

getConfigValueFromFile() {
    echo $(grep ^$2 $1 | cut -f 2- -d : | sed -e 's/#.*$//' | sed -e 's/^[[:space:]]*//')
}

getConfigValue() {
    local VALUE=$(getConfigValueFromFile local.yaml $1)
    [[ -z ${VALUE} ]] && { VALUE=$(getConfigValueFromFile config.yaml $1); }
    echo ${VALUE}
}

[[ -z ${XNAT_URL} ]] && \
    { XNAT_URL=$(getConfigValue xnat_url) || echo "XNAT_URL found"; }

[[ -z ${PIPELINE_URL} ]] && \
    { PIPELINE_URL=$(getConfigValue pipeline_url) || echo "PIPELINE_URL found"; }

GET_FRESH=Y
doDownload() {
    echo Downloading: $1
    curl -L --retry 5 --retry-delay 5 -O $1 || echo "Error downloading $1"
}

FOUND_FILE=""

# usage:
# findFirst file.txt /path/to/dir1 ./dir2 ./dir3/dir4
# returns first found path (or empty string if not found) as $firstFound var
findFirst() {

    FILENAME=$1

    shift

    # reset $firstFound var
    FOUND_FILE=""
	# return if we find it right away
	if [[ -e ${FILENAME} ]]; then
	    FOUND_FILE=${FILENAME}
	    return 0
	fi
	# or check the list of directories
	for dir in $@
	do
		if [[ -e ${dir}/${FILENAME} ]]; then
			FOUND_FILE=${dir}/${FILENAME}
			return 0
		fi
		continue
	done
	return 1
}

# search config folder, then 'local' folder then 'local/downloads'
DL_DIRS="./ ../../local ../../local/downloads"

XNAT_WAR_FILENAME=${XNAT_URL##*/}

findFirst ${XNAT_WAR_FILENAME} ${DL_DIRS}
XNAT_WAR=${FOUND_FILE}

if [[ -e ${XNAT_WAR} ]]; then
    read -p "${XNAT_WAR_FILENAME} has already been downloaded. Would you like to download a new copy? [Y/n] " GET_FRESH_XNAT_WAR
else
    GET_FRESH_XNAT_WAR=Y
fi

PIPELINE_ZIP_FILENAME=${PIPELINE_URL##*/}

findFirst ${PIPELINE_ZIP_FILENAME} ${DL_DIRS}
PIPELINE_ZIP=${FOUND_FILE}

if [[ -e ${PIPELINE_ZIP} ]]; then
    read -p "${PIPELINE_ZIP_FILENAME} has already been downloaded. Would you like to download a new copy? [Y/n] " GET_FRESH_PIPELINE_ZIP
else
    GET_FRESH_PIPELINE_ZIP=Y
fi

if [[ ! ${GET_FRESH_XNAT_WAR} =~ [Nn] ]]; then
    [[ -e ${XNAT_WAR} ]] && { rm ${XNAT_WAR}; }
    echo
    echo Downloading from configured URL: ${XNAT_URL}
    cd ../../local/downloads/
        doDownload ${XNAT_URL}
    cd -
fi

if [[ ! ${GET_FRESH_PIPELINE_ZIP} =~ [Nn] ]]; then
    [[ -e ${PIPELINE_ZIP} ]] && { rm ${PIPELINE_ZIP}; }
    echo
    echo Downloading from configured URL: ${PIPELINE_URL}
    cd ../../local/downloads/
        doDownload ${PIPELINE_URL}
    cd -
fi

echo
echo Starting XNAT build...

echo
echo Provisioning VM with specified user...

echo
vagrant up

echo
echo Reloading VM to configure folder sharing...

echo
vagrant reload

echo
echo Running build provision to build and deploy XNAT on the VM...

echo
vagrant provision --provision-with build

[[ -e ./.vagrant/machines/${NAME}/virtualbox/index_uuid ]] \
    && cp -fv ./.vagrant/machines/${NAME}/virtualbox/index_uuid ./.work/

echo
echo Provisioning completed.
