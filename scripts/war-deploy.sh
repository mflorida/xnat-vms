#!/bin/bash

#
# Download and deploy XNAT war
#

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript bin/macros
sourceScript defaults.sh

# We're really starting this script now.
echo Now running the "war-deploy.sh" provisioning script.

if [[ -e ~/.bash_profile ]]; then
    source ~/.bash_profile;
elif [[ -e ~/.bashrc ]]; then
    source ~/.bashrc;
fi

# Stop these services so that we can configure them later.
sudo service tomcat7 stop
sudo service nginx stop

# Configure the host settings.
echo -e "${VM_IP} ${HOST} ${SERVER}" | sudo tee --append /etc/hosts

# Configure nginx to proxy Tomcat.
replaceTokens xnatdev | sudo tee /etc/nginx/sites-available/${HOST}
sudo rm /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/${HOST} /etc/nginx/sites-enabled/${HOST}

sudo rm -rf /var/log/nginx/*

echo "Starting nginx..."
sudo service nginx start

# TOMCAT STUFF
CURRENT_USER=$(whoami)
setFolderOwner ${CURRENT_USER} /var/lib/tomcat7

setTomcatConfiguration

# Remove any existing web apps
rm -rf /var/lib/tomcat7/webapps/*

# Set up PostgreSQL
configurePostgreSQL

# XNAT STUFF

# Create project subfolders

# setup XNAT data folders
setupFolders ${DATA_ROOT} ${CURRENT_USER}

# make sure there's a 'universal' local/downloads folder
#mkdir -p /vagrant-root/local/downloads
DL_DIR=/vagrant
ROOT_DL_DIR=/vagrant-root/local/downloads

# Download pre-built .war file and copy to tomcat webapps folder
getWar(){

    URL=$1

    cd ${DATA_ROOT}/src

    # if the file has already been downloaded to the host, use that
    if [[ -f ${DL_DIR}/${URL##*/} && ${URL##*/} == *.war ]]; then
        cp ${DL_DIR}/${URL##*/} /var/lib/tomcat7/webapps/${CONTEXT}.war
    elif [[ -f ${ROOT_DL_DIR}/${URL##*/} && ${URL##*/} == *.war ]]; then
        cp ${ROOT_DL_DIR}/${URL##*/} /var/lib/tomcat7/webapps/${CONTEXT}.war
    else
        cd ${DL_DIR}
        echo
        echo "Downloading: ${URL}"
        curl -L --retry 5 --retry-delay 5 -s -O ${URL} \
        && cp ${DL_DIR}/${URL##*/} /var/lib/tomcat7/webapps/${CONTEXT}.war \
        || echo "Error downloading '${URL}'"
        cd -
    fi
}

# get the war file and copy it into the webapps folder
echo
echo Getting XNAT war file...
getWar ${XNAT_URL}


getPipeline() {

    URL=$1

    cd ${DATA_ROOT}/src

    [[ ! -d pipeline ]] && { mkdir pipeline; }
    cd pipeline

    # if the file has already been downloaded to the host, use that
    if [[ ! -f ${DL_DIR}/${URL##*/} ]]; then
        cd ${DL_DIR}
        echo
        echo "Downloading: ${URL}"
        curl -L --retry 5 --retry-delay 5 -s -O ${URL} \
        || echo "Error downloading '${URL}'"
        cd -
    fi

    if [[ -f ${DL_DIR}/${URL##*/} ]]; then
        echo Extracting ${URL##*/}...
        unzip -qo ${DL_DIR}/${URL##*/}
        [[ -d xnat-pipeline ]] && { mv xnat-pipeline/* .; rmdir xnat-pipeline; }
        replaceTokens pipeline.gradle.properties | tee gradle.properties
        sudo ./gradlew -q
    fi
}

# Get the pipeline zip file, extract it, and run the installer.
echo
echo Getting Pipeline zip file...
getPipeline ${PIPELINE_URL}

# Is the variable MODULES defined?
[[ -v MODULES ]] \
    && { echo Found MODULES set to ${MODULES}, pulling repositories.; /vagrant-root/scripts/pull_module_repos.rb ${DATA_ROOT}/modules ${MODULES}; } \
    || { echo No value set for the MODULES configuration, no custom functionality will be included.; }

# Search for any post-build execution folders and execute the install.sh
for POST_DIR in /vagrant/post_*; do
    if [[ -e ${POST_DIR}/install.sh ]]; then
        echo Executing post-processing script ${POST_DIR}/install.sh
        bash ${POST_DIR}/install.sh
    fi
done

sudo rm -rf /var/log/tomcat7/*

# Lastly, see if there's a public key configured.
if [[ ! -z ${PUBLIC_KEY} ]]; then
    # If so, add it to both Vagrant and XNAT users
    echo Adding public key to authorized keys for ${CURRENT_USER} and ${XNAT_USER}
    addAuthorizedPublicKey ~/.ssh/authorized_keys
    addAuthorizedPublicKey ${XNAT_HOME}/.ssh/authorized_keys
fi

# Set ownership of data and Tomcat folders to the XNAT user.
setFolderOwner ${XNAT_USER} /data ${DATA_ROOT} /var/lib/tomcat7

sudo chmod 755 /var/log/tomcat7
echo "Starting Tomcat..."
startTomcat
monitorTomcatStartup

STATUS=$?
if [[ ${STATUS} == 0 ]]; then
    # after setup is successful, specify 'reload' as the startup command
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "Your VM's IP address is ${VM_IP}. "
    echo "Your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
    exit 0;
else
    echo The application does not appear to have started properly. Status code: ${STATUS}
    echo The last lines in the log are:; tail -n 40 /var/log/tomcat7/catalina.out;
fi

exit ${STATUS}

