#!/bin/bash
#
# XNAT download and installation
#

# Suppress decorated or formatted output from gradle.
TERM=dumb

sourceScript() {
    test -f /vagrant/scripts/$1 && source /vagrant/scripts/$1 || source /vagrant-root/scripts/$1
}

# Now initialize the build environment from the config's vars.sh settings.
source /vagrant/.work/vars.sh

# look in config's scripts folder first, then try the multi root
sourceScript bin/macros
sourceScript defaults.sh

if [[ -e ~/.bash_profile ]]; then
    source ~/.bash_profile;
elif [[ -e ~/.bashrc ]]; then
    source ~/.bashrc;
fi

# We're really starting this script now.
echo Now running the "gradle-build.sh" provisioning script.

# Stop these services so that we can configure them later.
sudo service tomcat7 stop
sudo service nginx stop

# Configure the host settings.
echo -e "${VM_IP} ${HOST} ${SERVER}" | sudo tee --append /etc/hosts > /dev/null

# Configure nginx to proxy Tomcat.
replaceTokens xnatdev | sudo tee /etc/nginx/sites-available/${HOST} > /dev/null
sudo rm /etc/nginx/sites-enabled/default
sudo ln -s /etc/nginx/sites-available/${HOST} /etc/nginx/sites-enabled/${HOST}

sudo rm -rf /var/log/nginx/*

echo "Starting nginx..."
sudo service nginx start

# TOMCAT STUFF
CURRENT_USER=$(whoami)
setFolderOwner ${CURRENT_USER} /var/lib/tomcat7

setTomcatConfiguration

# Remove any existing web apps
rm -rf /var/lib/tomcat7/webapps/*

# Set up PostgreSQL
configurePostgreSQL

# XNAT STUFF

# setup XNAT data folders
setupFolders ${DATA_ROOT} ${CURRENT_USER}

# Copies or downloads specified release archive or folder
downloadSrc() {

    SRC=$1

    echo "Downloading ${SRC} to $(pwd)"
    wget -N -q -P ${DATA_ROOT}/src ${SRC} || echo "Error downloading '${SRC}'"

}

# clone dev source repo or copy a repo (folder) already downloaded locally
getDev() {

    SRC=$1
    BCH=$2
    DIR=$3
    COPIED=1

    SRC_DIR=${DATA_ROOT}/src/${DIR}

    # check to see if the src dir already exists
    if [[ -e ${SRC_DIR} ]]; then
        echo "The source repository already exists: ${SRC_DIR}"
        COPIED=0
    # clone with Git if SRC ends with .git
    elif [[ ${SRC} == *.git ]]; then
        echo "Cloning branch/tag '${BCH}' from '${SRC}' to '${SRC_DIR}'"
        # git clone --branch <tag_name> <repo_url> <dest_dir>
        git clone --branch ${BCH} ${SRC} ${SRC_DIR}
        COPIED=$?
    else
        echo "Downloading ${SRC}"
        downloadSrc ${SRC}
        COPIED=$?
    fi

    if [[ ${COPIED} != 0 ]]; then
        copyLocal ${SRC} ${SRC_DIR}
        COPIED=$?
        if [[ ${COPIED} != 0 ]]; then
            echo "Copy failed."
            exit;
        fi
    fi

    # in case the 'dev' source is an archive of the repo
#    echo Uncompressing ${SRC##*/}
    uncompress ${SRC##*/}
}

# get dev or release files
getDev ${XNAT_SRC} ${XNAT_BRANCH} ${XNAT};
# hacky conversion of _SRC to _DIR to handle differences
# between the _SRC parameter and the actual folder name
XNAT_DIR=${XNAT_SRC##*/}; XNAT_DIR=${XNAT_DIR%.tar.gz}; XNAT_DIR=${XNAT_DIR%.zip};

getDev ${PIPELINE_SRC} ${PIPELINE_BRANCH} ${PIPELINE_INST};

PIPE_DIR=${PIPELINE_SRC##*/}; PIPE_DIR=${PIPE_DIR%.tar.gz}; PIPE_DIR=${PIPE_DIR%.zip};

if [[ -d ${SRC_DIR} ]]; then
    replaceTokens pipeline.gradle.properties > ${SRC_DIR}/gradle.properties
    pushd ${SRC_DIR}
    sudo ./gradlew -q
    popd
fi

# Is the variable MODULES defined?
[[ -v MODULES ]] \
    && { echo Found MODULES set to ${MODULES}, pulling repositories.; /vagrant-root/scripts/pull_module_repos.rb ${DATA_ROOT}/modules ${MODULES}; } \
    || { echo No value set for the MODULES configuration, no custom functionality will be included.; }

# Gradle deployment setup
timestamp=$(date +%s);
mkdir -p ~/.gradle
[[ -e ~/.gradle/gradle.properties ]] && { mv ~/.gradle/gradle.properties ~/.gradle/gradle-${timestamp}.properties; }
echo '# ~/.gradle/gradle.properties' > ~/.gradle/gradle.properties
echo archiveName=ROOT >> ~/.gradle/gradle.properties
echo tomcatHome=/var/lib/tomcat7 >> ~/.gradle/gradle.properties

# Search for any post-build execution folders and execute the install.sh
for POST_DIR in /vagrant/post_*; do
    if [[ -e ${POST_DIR}/install.sh ]]; then
        echo Executing post-processing script ${POST_DIR}/install.sh
        bash ${POST_DIR}/install.sh
    fi
done

sudo rm -rf /var/log/tomcat7/*

# optionally run the Gradle build inside the VM
if [[ ! -z ${DEPLOY} && ${DEPLOY} == gradle-vm ]]; then
    echo "Starting Gradle build..."
    cd ${DATA_ROOT}/src/${XNAT}
    sudo ./gradlew war && { [ -e build/libs/xnat-web-*.war ] && { echo "Gradle build completed, deploying to Tomcat."; cp build/libs/xnat-web-*.war /var/lib/tomcat7/webapps/${CONTEXT}.war; } || { echo Ran the Gradle build, but there\'s no xnat-web war file in build/libs.; } } || die "Gradle build failed."
    cd -
elif [[ ! -z ${CONFIG} && ${CONFIG} == *dev* ]] || [[ ! -z ${DEPLOY} && ${DEPLOY} == war ]]; then
    # Or deploy the war file if it's available.
    echo "Looking for an existing XNAT war file in ${DATA_ROOT}/src/${XNAT}/build/libs"
    FOUND=false
    for FILE in ${DATA_ROOT}/src/${XNAT}/build/libs/*.war; do
        [[ -e "${FILE}" ]] && { echo "Installing ${FILE} as ${CONTEXT} web application."; cp "${FILE}" /var/lib/tomcat7/webapps/${CONTEXT}.war; }
        FOUND=true
        break
    done
    [[ ${FOUND} == false ]] && { echo No war file was found in the directory ${DATA_ROOT}/src/${XNAT}/build/libs, so XNAT won\'t be installed on start-up.; }
fi

# Lastly, see if there's a public key configured.
if [[ ! -z ${PUBLIC_KEY} ]]; then
    # If so, add it to both Vagrant and XNAT users
    echo Adding public key to authorized keys for ${CURRENT_USER} and ${XNAT_USER}
    addAuthorizedPublicKey ~/.ssh/authorized_keys
    addAuthorizedPublicKey ${XNAT_HOME}/.ssh/authorized_keys
fi

# Set ownership of data and Tomcat folders to the XNAT user.
setFolderOwner ${XNAT_USER} /data ${DATA_ROOT} /var/lib/tomcat7

sudo chmod 755 /var/log/tomcat7

# Skip Tomcat startup and quit early if no war file was deployed.
if [[ ! -e /var/lib/tomcat7/webapps/${CONTEXT}.war ]]; then
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "VM is ready. Please deploy XNAT war and start Tomcat."
    echo "Afterwards, your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
    exit 0;
fi

echo "Starting Tomcat..."
startTomcat
monitorTomcatStartup

STATUS=$?
if [[ ${STATUS} == 0 ]]; then
    # after setup is successful, specify 'reload' as the startup command
    printf "reload" > /vagrant/.work/startup
    echo "==========================================================="
    echo "Your VM's IP address is ${VM_IP}. "
    echo "Your XNAT server will be available at: "
    echo "${SITE_URL}"
    echo "==========================================================="
else
    echo The application does not appear to have started properly. Status code: ${STATUS}
    echo The last lines in the log are:; tail -n 40 /var/log/tomcat7/catalina.out;
fi

exit ${STATUS}
