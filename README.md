# XNAT Vagrant

This is the repository for the [XNAT](http://www.xnat.org) Vagrant project.

> **Note:** Windows users will need a Bash terminal program - **Cygwin** or **Git Bash** are recommended. Git Bash is installed by default when you run the Git installer and should work for running the scripts in this repo.

## Quick-Start

- Make sure you have [Git](https://git-scm.com/downloads), [Vagrant](https://www.vagrantup.com), and [VirtualBox](https://www.virtualbox.org) installed on your host machine. Currently the XNAT Vagrant project only supports the VirtualBox Vagrant provider.
- Clone the repo: `git clone https://bitbucket.org/xnatdev/xnat-vagrant.git`
- From inside the `xnat-vagrant` folder, run `./run xnat setup` to launch and configure a Vagrant VM using the [latest pre-built XNAT war file](https://bintray.com/nrgxnat/applications/XNAT/_latestVersion). Other VM configurations can be set up similarly, substituting the folder name of the config: `./run xnat-latest setup`, etc. You can see the available configurations by listing the contents of the **configs** folder. The currently available configurations are:

  - [xnat-release](configs/xnat-release) downloads the latest XNAT release bundle and installs it in the VM.
  - [xnat-dev](configs/xnat-dev) mounts XNAT source and pipeline folders into the VM to facilitate deploying XNAT development.
  - [xnat-latest](configs/xnat-latest) clones the XNAT web source folder to allow you to build the latest XNAT code inside the VM.
  - [xnat165-box](configs/xnat165-box) builds a VM using the XNAT 1.6.5 box image, which provides the XNAT 1.6.5 release server.

### List of commands:

- `./run xnat setup` - initial VM setup - this __must__ be performed first to create the VM
- `./run xnat stop` - shuts down the VM
- `./run xnat start` - (re)launches a VM that has been set up but is not running
- `./run xnat destroy` - deletes the VM and related files

The `run` script is more or less a proxy for the `vagrant` commands, allowing you to work with multiple VMs from a single 'root' folder. You can also navigate to each individual configuration folder and run the **setup.sh** scripts or the Vagrant commands directly.

```bash
$ cd configs/xnat
$ ./setup.sh
```

## Configuration

In each folder, you can set up a file named **local.yaml** to customize various attributes of your Vagrant VM. Each folder contains a version of **sample.local.yaml** that you can use as the starting point for your own **local.yaml** file. You can reference the **config.yaml** file in that configuration to see the default values that are passed into the Vagrant configuration.

### VM options

Many of the standard options in the **local.yaml** file are fairly straightforward and primarily affect basic traits about how the VM is configured and XNAT is initialized:

- **name:** The name of the VM. This is used as the VM name in the VirtualBox inventory.
- **host:** The host name to set for the VM. This defaults to the value of **name**.
- **server:** The server address for the VM. This defaults to the VM's IP address.
- **project:** The project ID. This is used for naming various items within XNAT. The default is **xnat**.
- **site:** The site ID.
- **xnat_user:** The name of the user account to create on the VM. This defaults to the value of the **project** setting.
- **xnat_pass:** The password to set for the VM user account. If you don't set a value for this setting, the password will not be set at all, meaning you can only access the VM through SSH public/private key pair authentication. Setting any value results in setting the password to that value.
- **vm_ip:** The IP address to set for the VM.
- **ram:** The amount of RAM to allocate for the VM. This defaults to 2048 MB.
- **cpus:** The number of CPU cores to allocate for the VM. This defaults to 1.

### Advanced XNAT configuration

As noted, there are a few options you can set in **local.yaml** that affect how XNAT itself is configured in the VM. To achieve more advanced configuration, for example configuring email and notification options, using an external existing database, and even mounting XNAT to use an existing data repository, you can mount shared folders that contain XNAT configuration and initialization files or data repositories.

The standard folders in the XNAT user's home folder are:

- **config** contains the XNAT configuration and initialization file(s)
- **logs** contains the XNAT application logs
- **plugins** contains any installed XNAT plugin libraries
- **work** contains temporary files for managing downloads, server work, etc.

The **xnat-vagrant** **.gitignore** file contains entries for these folders at the root level of the project, meaning you can create and mount these folders without them showing up as changes to the source-controlled project. To mount these folders on your VM, you need to configure them in a **shares** section in your **local.yaml** file. The code below demonstrates how to mount the **config** and **plugins** folders.

```bash
shares:
    '/path/to/config':
        - '/data/xnat/home/config'
        - ['fmode=644','dmode=755']
    '/path/to/plugins':
        - '/data/xnat/plugins'
        - ['fmode=644','dmode=755']
```

In this configuration, the first path, e.g. **/path/to/config**, indicates the location of a folder on your host or local machine. This means you can re-use a particular configuration for multiple VMs ((although this can cause issues if multiple XNATs are running against the same database).

#### Initializing the database from an SQL dump file

During the default provisioning of the XNAT Vagrant VM, a role and database are created for your XNAT server. The database role uses the same value as the user account, i.e. **xnat_user**, while the database is named with the value for the **project** setting. By default, the database itself is unpopulated until XNAT starts and creates its default data schemas. If you'd like to be able to start from a saved database state, you can specify the location of an SQL dump file with the **psql_import** option. Once the role and database are provisioned, the provisioning script checks for this value and, if set, imports it into the database.

> **Note:** The path indicated by the value specified for **pg_import** must be local to the virtual machine and not indicate a path on the host machine! You can put the SQL dump file into the folder from which you're building your Vagrant VM and reference it via something like **/vagrant/_dumpfile.sql_**. Alternatively, you could put the file in a folder on the host machine then mount that folder as a shared folder on the VM:

```bash
psql_import: /data/xnat/support/xnat.sql
shares:
    '/path/to/files':
        - '/data/xnat/support'
        - ['fmode=644','dmode=755']
```

You can create an SQL dump file from an existing XNAT installation or VM using the standard PostgreSQL command **pg_dump**. The command below shows how you could dump the database to SQL using the same file path as shown above:

```bash
$ pg_dump xnat > /data/xnat/support/xnat.sql
```

#### Reusing XNAT data archive folders across VM setups

You can save the data created by your Vagrant VM across destroy and setup operations by storing the XNAT data archive folder on the host machine, then sharing that folder to the VM through a shared folder:

```bash
shares:
    '/path/to/local/archive':
        - '/data/xnat/archive'
        - ['fmode=644','dmode=755']
```

Combined with dumping then restoring the database from an SQL dump file, this allows you to migrate server state across multiple iterations of your VM.

#### Configuring the VM user home folder

You can set up the VM user's home folder (e.g., **/data/xnat/home**) as a shared folder, but you must make sure you set it up properly.

> **Note:** An improperly configured home folder can cause lots of problems, including preventing you from accessing the VM through ssh. It's usually best to share the folders required to bootstrap your XNAT VM, such as **config**, **plugins**, and **archive**, so that you can re-use configuration and initialization files and existing data stores. Mounting the entire VM user folder allows you to configure the user's shell environment through custom **.bashrc** and **.bash_aliases**, provide custom tools through something like a local **bin** folder, and so on.

The configuration for using a shared home folder for your VM user should look something like this (this example uses the default VM user **xnat**):

```bash
shares:
    '../../../home':
        - '/data/xnat/home'
        - ['fmode=644','dmode=755']
    '../../../home/.ssh':
        - '/data/xnat/home/.ssh'
        - ['fmode=600','dmode=700']
```

This sharing configuration maps the **/data/xnat/home** folder to **../../../home** (in other words, a home folder located right outside of the **xnat-vagrant** source repository). It then maps the **/data/xnat/home/.ssh** folder to the **.ssh** folder inside the already shared **/data/xnat/home** folder. This is necessary because of the file and directory permissions set at each level:

- The VM user's home folder and its subfolders must have no greater access level than full user access with group and world read and execute access ([this post](http://blog.superuser.com/2011/04/22/linux-permissions-demystified/) has a good explanation of how the mode numbers above set the file and directory permissions). Restricting _all_ group and world access can make for a difficult working environment, though.
- The VM user's **.ssh** folder is a special case. Public/private key authentication will **not** be allowed if the permissions on the **.ssh** folder or the **authorized__key** file within that folder allow any group or world access.

You also must make sure that you include the correct version of **authorized__key** or you won't be able to access the VM through ssh.
